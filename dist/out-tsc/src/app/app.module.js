import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import 'rxjs/add/operator/share';
import { ApiService } from './services/api.service';
import { IonicStorageModule } from '@ionic/storage';
import { UserService } from './services/user.service';
import { Device } from '@ionic-native/device/ngx';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { CallDirectory } from '@ionic-native/call-directory/ngx';
import { CallLog } from '@ionic-native/call-log/ngx';
import { ModalpagePage } from './modalpage/modalpage.page';
import { Contacts } from '@ionic-native/contacts/ngx';
// import { Chooser } from '@ionic-native/chooser/ngx'; , Contact, ContactField, ContactName 
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib_1.__decorate([
        NgModule({
            declarations: [AppComponent, ModalpagePage],
            entryComponents: [ModalpagePage],
            //, Contact, ContactField, ContactName
            // import { Chooser } from '@ionic-native/chooser/ngx'; 
            imports: [BrowserModule,
                IonicModule.forRoot(),
                AppRoutingModule,
                BrowserModule /* or CommonModule */,
                FormsModule,
                ReactiveFormsModule,
                HttpClientModule,
                HttpModule,
                IonicStorageModule.forRoot()
            ],
            providers: [
                StatusBar,
                ApiService,
                UserService,
                SplashScreen,
                CallNumber,
                CallDirectory,
                Contacts,
                CallLog,
                // Contact,
                // ContactField,
                // ContactName,   
                // Chooser,
                Device,
                { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
            ],
            bootstrap: [AppComponent]
        })
    ], AppModule);
    return AppModule;
}());
export { AppModule };
//# sourceMappingURL=app.module.js.map