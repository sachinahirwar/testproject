import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { MenuController } from '@ionic/angular';
import { ApiService } from './../services/api.service';
var DashboardPage = /** @class */ (function () {
    function DashboardPage(route, router, api, routers, menuCtrl) {
        this.route = route;
        this.router = router;
        this.api = api;
        this.routers = routers;
        this.menuCtrl = menuCtrl;
    }
    DashboardPage.prototype.ngOnInit = function () {
        this.menuCtrl.enable(true);
        this.getdashboardtotal();
    };
    DashboardPage.prototype.ionViewWillEnter = function () {
        console.log('ionViewWillEnter dashboard');
        this.getdashboardtotal();
    };
    DashboardPage.prototype.getdashboardtotal = function () {
        var _this = this;
        console.log("abc");
        var test = 'dahsboard';
        var seq = this.api.post('applogin/getdashboardtotal', test).share();
        seq
            .map(function (res) { return res.json(); })
            .subscribe(function (res) {
            if (res.status == 'success') {
                _this.pending = res.pendingdata;
                _this.totalstd = res.totaldata;
                _this.notinstd = res.notintrested;
                _this.todaysenq = res.todaysenq;
            }
            else {
                console.log(res.error);
            }
        }, function (err) {
            console.error('ERROR', err);
        });
        return seq;
    };
    DashboardPage.prototype.todayfollowupstudent = function () {
        this.router.navigate(['/todayfollowupstudent']);
    };
    DashboardPage.prototype.pendingstudent = function () {
        this.router.navigate(['/pendingstudent']);
    };
    DashboardPage.prototype.intrestedstudent = function () {
        this.router.navigate(['/intrestedstudent']);
    };
    DashboardPage = tslib_1.__decorate([
        Component({
            selector: 'app-dashboard',
            templateUrl: './dashboard.page.html',
            styleUrls: ['./dashboard.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [ActivatedRoute,
            Router,
            ApiService,
            Router,
            MenuController])
    ], DashboardPage);
    return DashboardPage;
}());
export { DashboardPage };
//# sourceMappingURL=dashboard.page.js.map