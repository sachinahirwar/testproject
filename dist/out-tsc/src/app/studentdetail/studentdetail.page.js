import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { FormBuilder, Validators } from '@angular/forms';
import { UserService } from './../services/user.service';
import { ApiService } from './../services/api.service';
import { ToastController, NavController, LoadingController } from '@ionic/angular';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/share';
import 'rxjs/Rx';
import { Storage } from '@ionic/storage';
import { CallNumber } from '@ionic-native/call-number/ngx';
var StudentdetailPage = /** @class */ (function () {
    function StudentdetailPage(route, loadingController, router, fb, userService, api, toastController, navCtrl, storage, user, callNumber) {
        var _this = this;
        this.route = route;
        this.loadingController = loadingController;
        this.router = router;
        this.fb = fb;
        this.userService = userService;
        this.api = api;
        this.toastController = toastController;
        this.navCtrl = navCtrl;
        this.storage = storage;
        this.user = user;
        this.callNumber = callNumber;
        this.buttonColor = "primary";
        this.route.queryParams.subscribe(function (params) {
            _this.stdid = params["studentid"];
            _this.getstudentdata(_this.stdid);
            console.log('get student id from todayfollowupstudent', _this.stdid);
        });
        this.loginForm = fb.group({
            remark: [this.remark, [Validators.required]],
            status: ['', [Validators.required]],
            date: ['', [Validators.required]],
        });
    }
    // this.getstudentdata();
    StudentdetailPage.prototype.ngOnInit = function () {
    };
    StudentdetailPage.prototype.ionViewWillEnter = function () {
        console.log('ionViewWillEnter');
    };
    StudentdetailPage.prototype.studentcall = function (stmobile) {
        this.buttonColor = "success";
        console.log("student calling....", stmobile);
        this.callNumber.callNumber(stmobile, true)
            .then(function (res) { return console.log('Launched dialer!', res); })
            .catch(function (err) { return console.log('Error launching dialer', err); });
    };
    StudentdetailPage.prototype.getstudentdata = function (id) {
        var _this = this;
        console.log('getstudentdata funtion id', id);
        var getstudentid = { id1: id };
        var seq = this.api.post('applogin/getstudentdata', getstudentid).share();
        seq
            .map(function (res) { return res.json(); })
            .subscribe(function (res) {
            if (res.status == 'success') {
                _this.stdname = res.name;
                _this.stdcourse = res.course;
                _this.stdmob = res.mobile;
                _this.remark = res.remark;
                _this.returnstdid = res.id;
                console.log('mahesh billore', _this.returnstdid);
                // this.studentlist=res.todayfollowupstudent;
                // this.router.navigate(['/dashboard']);
            }
            else {
                console.log(res.error);
            }
        }, function (err) {
            console.error('ERROR', err);
        });
        return seq;
    };
    StudentdetailPage.prototype.savestatus = function (loginForm) {
        var _this = this;
        console.log("status data", loginForm);
        console.log("status id", this.returnstdid);
        var loginForm1 = { 'remark': loginForm['remark'], 'loginForm': loginForm['status'], 'date': loginForm['date'], 'stdid': this.stdid };
        // this.logincontroller();
        var seq = this.api.post('applogin/savestatus', loginForm1).share();
        seq
            .map(function (res) { return res.json(); })
            .subscribe(function (res) {
            if (res.status == 'success') {
                console.log(res.user);
                _this.router.navigate(['/dashboard']);
            }
            else {
                console.log(res.error);
            }
        }, function (err) {
            console.error('ERROR', err);
        });
        return seq;
    };
    StudentdetailPage = tslib_1.__decorate([
        Component({
            selector: 'app-studentdetail',
            templateUrl: './studentdetail.page.html',
            styleUrls: ['./studentdetail.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [ActivatedRoute, LoadingController, Router, FormBuilder, UserService, ApiService,
            ToastController, NavController, Storage, UserService, CallNumber])
    ], StudentdetailPage);
    return StudentdetailPage;
}());
export { StudentdetailPage };
//# sourceMappingURL=studentdetail.page.js.map