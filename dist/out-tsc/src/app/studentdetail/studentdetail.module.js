import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { StudentdetailPage } from './studentdetail.page';
var routes = [
    {
        path: '',
        component: StudentdetailPage
    }
];
var StudentdetailPageModule = /** @class */ (function () {
    function StudentdetailPageModule() {
    }
    StudentdetailPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                ReactiveFormsModule,
                RouterModule.forChild(routes)
            ],
            declarations: [StudentdetailPage]
        })
    ], StudentdetailPageModule);
    return StudentdetailPageModule;
}());
export { StudentdetailPageModule };
//# sourceMappingURL=studentdetail.module.js.map