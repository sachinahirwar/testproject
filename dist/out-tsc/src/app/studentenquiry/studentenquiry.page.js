import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { UserService } from './../services/user.service';
import { ApiService } from './../services/api.service';
import { ToastController, NavController, LoadingController, Platform } from '@ionic/angular';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/share';
import 'rxjs/Rx';
import { Storage } from '@ionic/storage';
import { CallDirectory } from '@ionic-native/call-directory/ngx';
import { Contacts } from '@ionic-native/contacts/ngx';
import { CallLog } from '@ionic-native/call-log/ngx';
// import { Contacts, Contact, ContactField, ContactName } from '@ionic-native/contacts/ngx';
import { Router, ActivatedRoute } from "@angular/router";
var StudentenquiryPage = /** @class */ (function () {
    function StudentenquiryPage(loadingController, router, fb, userService, api, toastController, callDirectory, navCtrl, storage, user, contacts, callLog, platform, route) {
        var _this = this;
        this.loadingController = loadingController;
        this.router = router;
        this.fb = fb;
        this.userService = userService;
        this.api = api;
        this.toastController = toastController;
        this.callDirectory = callDirectory;
        this.navCtrl = navCtrl;
        this.storage = storage;
        this.user = user;
        this.contacts = contacts;
        this.callLog = callLog;
        this.platform = platform;
        this.route = route;
        this.mobile = '';
        this.contactname = '';
        this.route.queryParams.subscribe(function (params) {
            if (params["mobilenumber"] != '') {
                _this.mobile = params["mobilenumber"];
                console.log('mobilenumber', _this.mobile);
            }
            // console.log(this.catid);
        });
        this.loginForm = fb.group({
            name: ['', [Validators.required]],
            course: ['', [Validators.required]],
            mobile: [this.mobile, [Validators.required]],
            date: ['', [Validators.required]],
            remark: ['', [Validators.required]],
            operatorid: ['', [Validators.required]]
        });
        this.platform.ready().then(function () {
            _this.callLog.hasReadPermission().then(function (hasPermission) {
                if (!hasPermission) {
                    _this.callLog.requestReadPermission().then(function (results) {
                        _this.getContacts("type", "1", "==");
                        console.log('listTyle', _this.getContacts("type", "1", "=="));
                    })
                        .catch(function (e) { return alert(" requestReadPermission " + JSON.stringify(e)); });
                }
                else {
                    _this.getContacts("type", "1", "==");
                }
            })
                .catch(function (e) { return alert(" hasReadPermission " + JSON.stringify(e)); });
        });
    }
    StudentenquiryPage.prototype.ngOnInit = function () {
        this.getusers();
    };
    StudentenquiryPage.prototype.getContacts = function (name, value, operator) {
        var _this = this;
        if (value == '1') {
            this.listTyle = "Incoming Calls from yesterday";
            console.log('listTyle', this.listTyle);
        }
        else if (value == '2') {
            this.listTyle = "Ougoing Calls from yesterday";
        }
        else if (value == '5') {
            this.listTyle = "Rejected Calls from yesterday";
        }
        //Getting Yesterday Time
        var today = new Date();
        var yesterday = new Date(today);
        yesterday.setDate(today.getDate() - 5);
        var fromTime = yesterday.getTime();
        this.filters = [{
                name: name,
                value: value,
                operator: operator,
            }, {
                name: "date",
                value: fromTime.toString(),
                operator: ">=",
            }];
        this.callLog.getCallLog(this.filters)
            .then(function (results) {
            _this.recordsFoundText = JSON.stringify(results);
            _this.recordsFound = results; //JSON.stringify(results);
            console.log('recordfound', _this.recordsFound);
            console.log('recordfoundTeaat', _this.recordsFoundText);
        })
            .catch(function (e) { return alert(" LOG " + JSON.stringify(e)); });
    };
    StudentenquiryPage.prototype.saveenquiry = function (loginForm) {
        var _this = this;
        console.log("enquiry data", loginForm);
        // this.logincontroller();
        var seq = this.api.post('applogin/savestudentenquiry', loginForm).share();
        seq
            .map(function (res) { return res.json(); })
            .subscribe(function (res) {
            if (res.status == 'success') {
                console.log(res.user);
                _this.router.navigate(['/dashboard']);
            }
            else {
                console.log(res.error);
            }
        }, function (err) {
            console.error('ERROR', err);
        });
        return seq;
    };
    StudentenquiryPage.prototype.opencalldirector = function () {
        var _this = this;
        this.contacts.pickContact().then(function (contact) {
            var contactNumber = contact.phoneNumbers[0].value;
            var contactname = contact.displayName;
            // var contactName=contact.ContactName[0].value;
            _this.mobile = contactNumber;
            _this.contactname = contactname;
            // alert(contactNumber);
            // alert(contact);
            console.log('contact name', contact.displayName);
            console.log('contact details', contact);
        }, function (err) {
            alert(JSON.stringify(err));
        });
    };
    StudentenquiryPage.prototype.getusers = function () {
        var _this = this;
        var getstudentid = 'id';
        var seq = this.api.post('applogin/getusers', getstudentid).share();
        seq
            .map(function (res) { return res.json(); })
            .subscribe(function (res) {
            if (res.status == 'success') {
                console.log('operators', res.operators);
                _this.operators = res.operators;
            }
            else {
                console.log(res.error);
            }
        }, function (err) {
            console.error('ERROR', err);
        });
        return seq;
    };
    StudentenquiryPage = tslib_1.__decorate([
        Component({
            selector: 'app-studentenquiry',
            templateUrl: './studentenquiry.page.html',
            styleUrls: ['./studentenquiry.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [LoadingController,
            Router,
            FormBuilder,
            UserService,
            ApiService,
            ToastController,
            CallDirectory,
            NavController,
            Storage,
            UserService,
            Contacts,
            CallLog,
            Platform,
            ActivatedRoute])
    ], StudentenquiryPage);
    return StudentenquiryPage;
}());
export { StudentenquiryPage };
//# sourceMappingURL=studentenquiry.page.js.map