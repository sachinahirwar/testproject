import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { StudentenquiryPage } from './studentenquiry.page';
var routes = [
    {
        path: '',
        component: StudentenquiryPage
    }
];
var StudentenquiryPageModule = /** @class */ (function () {
    function StudentenquiryPageModule() {
    }
    StudentenquiryPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                ReactiveFormsModule,
                RouterModule.forChild(routes)
            ],
            declarations: [StudentenquiryPage]
        })
    ], StudentenquiryPageModule);
    return StudentenquiryPageModule;
}());
export { StudentenquiryPageModule };
//# sourceMappingURL=studentenquiry.module.js.map