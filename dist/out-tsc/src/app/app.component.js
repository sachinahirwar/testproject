import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from "@angular/router";
import { Storage } from '@ionic/storage';
import { UserService } from './services/user.service';
import { NavController } from '@ionic/angular';
var AppComponent = /** @class */ (function () {
    function AppComponent(platform, splashScreen, statusBar, router, storage, user, navCtrl) {
        this.platform = platform;
        this.splashScreen = splashScreen;
        this.statusBar = statusBar;
        this.router = router;
        this.storage = storage;
        this.user = user;
        this.navCtrl = navCtrl;
        this.appPages = [
            {
                title: 'dashboard',
                url: '/dashboard',
                icon: 'grid'
            },
            {
                title: 'Student Enquiry',
                url: '/studentenquiry',
                icon: 'search'
            },
            {
                title: 'Unknown Number',
                url: '/incommingcalls',
                icon: 'call'
            },
            {
                title: 'followup History',
                url: '/followuphistory',
                icon: 'person'
            },
            {
                title: 'Profile',
                url: '/profile',
                icon: 'person'
            },
            {
                title: 'Logout',
                url: '/login',
                icon: 'log-in'
            },
        ];
        this.initializeApp();
    }
    AppComponent.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready().then(function () {
            _this.storage.get('_user').then(function (_user) {
                if (_user) {
                    _this.user._loggedIn(_user);
                    _this.navCtrl.navigateRoot("dashboard");
                }
                else {
                    _this.navCtrl.navigateRoot(['/login']);
                }
            }, function (error) {
            });
            _this.statusBar.styleDefault();
            _this.statusBar.overlaysWebView(false);
            _this.statusBar.backgroundColorByHexString('#0cd1e8');
            _this.splashScreen.hide();
        });
    };
    AppComponent = tslib_1.__decorate([
        Component({
            selector: 'app-root',
            templateUrl: 'app.component.html'
        }),
        tslib_1.__metadata("design:paramtypes", [Platform,
            SplashScreen,
            StatusBar,
            Router,
            Storage,
            UserService,
            NavController])
    ], AppComponent);
    return AppComponent;
}());
export { AppComponent };
//# sourceMappingURL=app.component.js.map