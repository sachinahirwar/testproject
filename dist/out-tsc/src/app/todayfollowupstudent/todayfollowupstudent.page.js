import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Router } from "@angular/router";
import { FormBuilder } from '@angular/forms';
import { UserService } from './../services/user.service';
import { ApiService } from './../services/api.service';
import { ToastController, NavController, LoadingController } from '@ionic/angular';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/share';
import 'rxjs/Rx';
import { Storage } from '@ionic/storage';
var TodayfollowupstudentPage = /** @class */ (function () {
    function TodayfollowupstudentPage(loadingController, router, fb, userService, api, toastController, navCtrl, storage, user) {
        this.loadingController = loadingController;
        this.router = router;
        this.fb = fb;
        this.userService = userService;
        this.api = api;
        this.toastController = toastController;
        this.navCtrl = navCtrl;
        this.storage = storage;
        this.user = user;
        this.gettodayfollowupstudent();
        // console.log('mahesh billore',this.studentlist);
    }
    TodayfollowupstudentPage.prototype.ngOnInit = function () {
    };
    TodayfollowupstudentPage.prototype.gettodayfollowupstudent = function () {
        var _this = this;
        var studentstuatus = 'todaydate';
        var seq = this.api.post('applogin/gettodayfollowupstudent', studentstuatus).share();
        seq
            .map(function (res) { return res.json(); })
            .subscribe(function (res) {
            if (res.status == 'success') {
                console.log('mahesh billore', res.todayfollowupstudent);
                _this.studentlist = res.todayfollowupstudent;
                // this.router.navigate(['/dashboard']);
            }
            else {
                console.log(res.error);
            }
        }, function (err) {
            console.error('ERROR', err);
        });
        return seq;
    };
    TodayfollowupstudentPage.prototype.studentdetail = function (id) {
        // console.log(id);
        this.router.navigate(['/studentdetail'], { queryParams: { "studentid": id } });
        // this.router.navigate(['/studentdetail']{'id'=>id});
    };
    TodayfollowupstudentPage = tslib_1.__decorate([
        Component({
            selector: 'app-todayfollowupstudent',
            templateUrl: './todayfollowupstudent.page.html',
            styleUrls: ['./todayfollowupstudent.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [LoadingController, Router, FormBuilder, UserService, ApiService,
            ToastController, NavController, Storage, UserService])
    ], TodayfollowupstudentPage);
    return TodayfollowupstudentPage;
}());
export { TodayfollowupstudentPage };
//# sourceMappingURL=todayfollowupstudent.page.js.map