import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { TodayfollowupstudentPage } from './todayfollowupstudent.page';
var routes = [
    {
        path: '',
        component: TodayfollowupstudentPage
    }
];
var TodayfollowupstudentPageModule = /** @class */ (function () {
    function TodayfollowupstudentPageModule() {
    }
    TodayfollowupstudentPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [TodayfollowupstudentPage]
        })
    ], TodayfollowupstudentPageModule);
    return TodayfollowupstudentPageModule;
}());
export { TodayfollowupstudentPageModule };
//# sourceMappingURL=todayfollowupstudent.module.js.map