import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Router } from "@angular/router";
import { FormBuilder } from '@angular/forms';
import { UserService } from './../services/user.service';
import { ApiService } from './../services/api.service';
import { ToastController, NavController, LoadingController } from '@ionic/angular';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/share';
import 'rxjs/Rx';
import { Storage } from '@ionic/storage';
var PendingstudentPage = /** @class */ (function () {
    function PendingstudentPage(loadingController, router, fb, userService, api, toastController, navCtrl, storage, user) {
        this.loadingController = loadingController;
        this.router = router;
        this.fb = fb;
        this.userService = userService;
        this.api = api;
        this.toastController = toastController;
        this.navCtrl = navCtrl;
        this.storage = storage;
        this.user = user;
        this.getpendingstudent();
        // console.log('mahesh billore',this.studentlist);
    }
    PendingstudentPage.prototype.ngOnInit = function () {
    };
    PendingstudentPage.prototype.getpendingstudent = function () {
        var _this = this;
        var studentstuatus = 'pendingstudent';
        var seq = this.api.post('applogin/getpendingstudent', studentstuatus).share();
        seq
            .map(function (res) { return res.json(); })
            .subscribe(function (res) {
            if (res.status == 'success') {
                console.log('mahesh billore', res.pendingstd);
                _this.studentlist = res.pendingstd;
                // this.router.navigate(['/dashboard']);
            }
            else {
                console.log(res.error);
            }
        }, function (err) {
            console.error('ERROR', err);
        });
        return seq;
    };
    PendingstudentPage = tslib_1.__decorate([
        Component({
            selector: 'app-pendingstudent',
            templateUrl: './pendingstudent.page.html',
            styleUrls: ['./pendingstudent.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [LoadingController,
            Router,
            FormBuilder,
            UserService,
            ApiService,
            ToastController,
            NavController,
            Storage,
            UserService])
    ], PendingstudentPage);
    return PendingstudentPage;
}());
export { PendingstudentPage };
//# sourceMappingURL=pendingstudent.page.js.map