import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { PendingstudentPage } from './pendingstudent.page';
var routes = [
    {
        path: '',
        component: PendingstudentPage
    }
];
var PendingstudentPageModule = /** @class */ (function () {
    function PendingstudentPageModule() {
    }
    PendingstudentPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [PendingstudentPage]
        })
    ], PendingstudentPageModule);
    return PendingstudentPageModule;
}());
export { PendingstudentPageModule };
//# sourceMappingURL=pendingstudent.module.js.map