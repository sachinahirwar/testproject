import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { ApiService } from './../services/api.service';
import { ModalController } from '@ionic/angular';
import { ModalpagePage } from '../modalpage/modalpage.page';
var FollowuphistoryPage = /** @class */ (function () {
    function FollowuphistoryPage(api, modal) {
        this.api = api;
        this.modal = modal;
    }
    FollowuphistoryPage.prototype.ngOnInit = function () {
        this.getfolloupstudent();
    };
    FollowuphistoryPage.prototype.getfolloupstudent = function () {
        var _this = this;
        var getstudentid = 'id';
        var seq = this.api.post('applogin/getfolloupstudent', getstudentid).share();
        seq
            .map(function (res) { return res.json(); })
            .subscribe(function (res) {
            if (res.status == 'success') {
                console.log('operators', res);
                _this.todayfollowupstudent = res.todayfollowupstudent;
                _this.followuphistory = res.followuphistory;
            }
            else {
                console.log(res.error);
            }
        }, function (err) {
            console.error('ERROR', err);
        });
        return seq;
    };
    FollowuphistoryPage.prototype.openmodal = function (id) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var mymodal;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log(id);
                        return [4 /*yield*/, this.modal.create({
                                component: ModalpagePage,
                                componentProps: {
                                    'prop1': id
                                }
                            })];
                    case 1:
                        mymodal = _a.sent();
                        return [4 /*yield*/, mymodal.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    FollowuphistoryPage = tslib_1.__decorate([
        Component({
            selector: 'app-followuphistory',
            templateUrl: './followuphistory.page.html',
            styleUrls: ['./followuphistory.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [ApiService, ModalController])
    ], FollowuphistoryPage);
    return FollowuphistoryPage;
}());
export { FollowuphistoryPage };
//# sourceMappingURL=followuphistory.page.js.map