import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { FollowuphistoryPage } from './followuphistory.page';
var routes = [
    {
        path: '',
        component: FollowuphistoryPage
    }
];
var FollowuphistoryPageModule = /** @class */ (function () {
    function FollowuphistoryPageModule() {
    }
    FollowuphistoryPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                ReactiveFormsModule,
                RouterModule.forChild(routes)
            ],
            declarations: [FollowuphistoryPage]
        })
    ], FollowuphistoryPageModule);
    return FollowuphistoryPageModule;
}());
export { FollowuphistoryPageModule };
//# sourceMappingURL=followuphistory.module.js.map