import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { IntrestedstudentPage } from './intrestedstudent.page';
var routes = [
    {
        path: '',
        component: IntrestedstudentPage
    }
];
var IntrestedstudentPageModule = /** @class */ (function () {
    function IntrestedstudentPageModule() {
    }
    IntrestedstudentPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [IntrestedstudentPage]
        })
    ], IntrestedstudentPageModule);
    return IntrestedstudentPageModule;
}());
export { IntrestedstudentPageModule };
//# sourceMappingURL=intrestedstudent.module.js.map