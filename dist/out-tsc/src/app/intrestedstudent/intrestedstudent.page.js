import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Router } from "@angular/router";
import { FormBuilder } from '@angular/forms';
import { UserService } from './../services/user.service';
import { ApiService } from './../services/api.service';
import { ToastController, NavController, LoadingController } from '@ionic/angular';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/share';
import 'rxjs/Rx';
import { Storage } from '@ionic/storage';
var IntrestedstudentPage = /** @class */ (function () {
    function IntrestedstudentPage(loadingController, router, fb, userService, api, toastController, navCtrl, storage, user) {
        this.loadingController = loadingController;
        this.router = router;
        this.fb = fb;
        this.userService = userService;
        this.api = api;
        this.toastController = toastController;
        this.navCtrl = navCtrl;
        this.storage = storage;
        this.user = user;
        this.getintrestedstudent();
        // console.log('mahesh billore',this.studentlist);
    }
    IntrestedstudentPage.prototype.ngOnInit = function () {
    };
    IntrestedstudentPage.prototype.getintrestedstudent = function () {
        var _this = this;
        var studentstuatus = 'intrested';
        var seq = this.api.post('applogin/getintrestedstudent', studentstuatus).share();
        seq
            .map(function (res) { return res.json(); })
            .subscribe(function (res) {
            if (res.status == 'success') {
                console.log('mahesh billore', res.intrestedstd);
                _this.studentlist = res.intrestedstd;
                // this.router.navigate(['/dashboard']);
            }
            else {
                console.log(res.error);
            }
        }, function (err) {
            console.error('ERROR', err);
        });
        return seq;
    };
    IntrestedstudentPage = tslib_1.__decorate([
        Component({
            selector: 'app-intrestedstudent',
            templateUrl: './intrestedstudent.page.html',
            styleUrls: ['./intrestedstudent.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [LoadingController,
            Router,
            FormBuilder,
            UserService,
            ApiService,
            ToastController,
            NavController,
            Storage,
            UserService])
    ], IntrestedstudentPage);
    return IntrestedstudentPage;
}());
export { IntrestedstudentPage };
//# sourceMappingURL=intrestedstudent.page.js.map