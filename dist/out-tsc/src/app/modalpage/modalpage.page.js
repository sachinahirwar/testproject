import * as tslib_1 from "tslib";
import { Component, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { NavParams } from '@ionic/angular';
import { ApiService } from './../services/api.service';
var ModalpagePage = /** @class */ (function () {
    // public value = this.navParams.get('prop1');
    function ModalpagePage(api, modal, navParams) {
        this.api = api;
        this.modal = modal;
        this.navParams = navParams;
        // const instance = create(ModalpagePage);
        //  instance.prop1 = id;
        console.log(this.navParams.get('prop1'));
        this.id = this.navParams.get('prop1');
    }
    ModalpagePage.prototype.ngOnInit = function () {
        this.getstudenthistorydetail();
    };
    ModalpagePage.prototype.getstudenthistorydetail = function () {
        var _this = this;
        var getstudentid = { id: this.id };
        console.log("jgvhjbhj", getstudentid);
        var seq = this.api.post('applogin/getstudenthistorydetail', getstudentid).share();
        seq
            .map(function (res) { return res.json(); })
            .subscribe(function (res) {
            if (res.status == 'success') {
                console.log('operators', res);
                if (res != "") {
                    _this.studenthistory = res.getdata;
                    _this.stdname = res.getdata[0].name;
                }
                // this.followuphistory=res.followuphistory;
            }
            else {
                console.log(res.error);
            }
        }, function (err) {
            console.error('ERROR', err);
        });
        return seq;
    };
    ModalpagePage.prototype.closemodal = function () {
        this.modal.dismiss();
    };
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Object)
    ], ModalpagePage.prototype, "prop1", void 0);
    ModalpagePage = tslib_1.__decorate([
        Component({
            selector: 'app-modalpage',
            templateUrl: './modalpage.page.html',
            styleUrls: ['./modalpage.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [ApiService, ModalController, NavParams])
    ], ModalpagePage);
    return ModalpagePage;
}());
export { ModalpagePage };
//# sourceMappingURL=modalpage.page.js.map