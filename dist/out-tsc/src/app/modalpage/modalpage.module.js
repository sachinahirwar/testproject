import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { ModalpagePage } from './modalpage.page';
var routes = [
    {
        path: '',
        component: ModalpagePage
    }
];
var ModalpagePageModule = /** @class */ (function () {
    function ModalpagePageModule() {
    }
    ModalpagePageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [ModalpagePage]
        })
    ], ModalpagePageModule);
    return ModalpagePageModule;
}());
export { ModalpagePageModule };
//# sourceMappingURL=modalpage.module.js.map