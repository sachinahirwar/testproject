import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { IncommingcallsPage } from './incommingcalls.page';
var routes = [
    {
        path: '',
        component: IncommingcallsPage
    }
];
var IncommingcallsPageModule = /** @class */ (function () {
    function IncommingcallsPageModule() {
    }
    IncommingcallsPageModule = tslib_1.__decorate([
        NgModule({
            imports: [
                CommonModule,
                FormsModule,
                IonicModule,
                RouterModule.forChild(routes)
            ],
            declarations: [IncommingcallsPage]
        })
    ], IncommingcallsPageModule);
    return IncommingcallsPageModule;
}());
export { IncommingcallsPageModule };
//# sourceMappingURL=incommingcalls.module.js.map