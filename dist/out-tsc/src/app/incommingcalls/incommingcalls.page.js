import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { CallLog } from '@ionic-native/call-log/ngx';
import { Platform } from '@ionic/angular';
import { Router } from "@angular/router";
var IncommingcallsPage = /** @class */ (function () {
    function IncommingcallsPage(callLog, platform, router) {
        var _this = this;
        this.callLog = callLog;
        this.platform = platform;
        this.router = router;
        this.mobile = '';
        this.contactname = '';
        this.platform.ready().then(function () {
            _this.callLog.hasReadPermission().then(function (hasPermission) {
                if (!hasPermission) {
                    _this.callLog.requestReadPermission().then(function (results) {
                        _this.getContacts("type", "1", "==");
                        console.log('listTyle', _this.getContacts("type", "1", "=="));
                    })
                        .catch(function (e) { return alert(" requestReadPermission " + JSON.stringify(e)); });
                }
                else {
                    _this.getContacts("type", "1", "==");
                }
            })
                .catch(function (e) { return alert(" hasReadPermission " + JSON.stringify(e)); });
        });
    }
    IncommingcallsPage.prototype.ngOnInit = function () {
    };
    IncommingcallsPage.prototype.getContacts = function (name, value, operator) {
        var _this = this;
        if (value == '1') {
            this.listTyle = "Incoming Calls from yesterday";
            console.log('listTyle', this.listTyle);
        }
        else if (value == '2') {
            this.listTyle = "Ougoing Calls from yesterday";
        }
        else if (value == '5') {
            this.listTyle = "Rejected Calls from yesterday";
        }
        //Getting Yesterday Time
        var today = new Date();
        var yesterday = new Date(today);
        yesterday.setDate(today.getDate() - 5);
        var fromTime = yesterday.getTime();
        this.filters = [{
                name: name,
                value: value,
                operator: operator,
            }, {
                name: "date",
                value: fromTime.toString(),
                operator: ">=",
            }];
        this.callLog.getCallLog(this.filters)
            .then(function (results) {
            _this.recordsFoundText = JSON.stringify(results);
            _this.recordsFound = results; //JSON.stringify(results);
            console.log('recordfound', _this.recordsFound);
            console.log('recordfoundTeaat', _this.recordsFoundText);
        })
            .catch(function (e) { return alert(" LOG " + JSON.stringify(e)); });
    };
    IncommingcallsPage.prototype.goenquiry = function (name, number) {
        // console.log(name,number); 
        this.router.navigate(['/studentenquiry'], { queryParams: { "mobilenumber": number } });
    };
    IncommingcallsPage = tslib_1.__decorate([
        Component({
            selector: 'app-incommingcalls',
            templateUrl: './incommingcalls.page.html',
            styleUrls: ['./incommingcalls.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [CallLog,
            Platform,
            Router])
    ], IncommingcallsPage);
    return IncommingcallsPage;
}());
export { IncommingcallsPage };
//# sourceMappingURL=incommingcalls.page.js.map