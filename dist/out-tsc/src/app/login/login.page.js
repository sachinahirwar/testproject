import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { Router } from "@angular/router";
import { FormBuilder, Validators } from '@angular/forms';
import { UserService } from './../services/user.service';
import { ApiService } from './../services/api.service';
import { ToastController, NavController, LoadingController, MenuController } from '@ionic/angular';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/share';
import 'rxjs/Rx';
import { Storage } from '@ionic/storage';
var LoginPage = /** @class */ (function () {
    function LoginPage(loadingController, router, fb, userService, api, toastController, navCtrl, storage, user, menuCtrl) {
        this.loadingController = loadingController;
        this.router = router;
        this.fb = fb;
        this.userService = userService;
        this.api = api;
        this.toastController = toastController;
        this.navCtrl = navCtrl;
        this.storage = storage;
        this.user = user;
        this.menuCtrl = menuCtrl;
        this.loginForm = fb.group({
            name: ['', [Validators.required]],
            password: ['', [Validators.required, Validators.minLength(6)]]
        });
        this.user.logout();
    }
    LoginPage.prototype.ngOnInit = function () {
        this.menuCtrl.enable(false);
    };
    LoginPage.prototype.loginpost = function (loginForm) {
        var _this = this;
        console.log("test", loginForm);
        // this.logincontroller();
        var seq = this.api.post('applogin', loginForm).share();
        seq
            .map(function (res) { return res.json(); })
            .subscribe(function (res) {
            if (res.status == 'success') {
                //   this.presentToast(); 
                console.log('responce mahesh', res);
                _this.user.setDestn(res);
                console.log('responce', _this.user._user);
                _this.router.navigate(['/dashboard']);
                // this.router.navigate(['/dashboard']);
            }
            else {
                // this.presentToastError();
                console.log(res.error);
            }
        }, function (err) {
            console.error('ERROR', err);
        });
        return seq;
    };
    LoginPage = tslib_1.__decorate([
        Component({
            selector: 'app-login',
            templateUrl: './login.page.html',
            styleUrls: ['./login.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [LoadingController, Router, FormBuilder, UserService, ApiService,
            ToastController, NavController, Storage, UserService, MenuController])
    ], LoginPage);
    return LoginPage;
}());
export { LoginPage };
//# sourceMappingURL=login.page.js.map