import * as tslib_1 from "tslib";
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
// Typescript custom enum for search types (optional)
export var SearchType;
(function (SearchType) {
    SearchType["all"] = "";
    SearchType["movie"] = "movie";
    SearchType["series"] = "series";
    SearchType["episode"] = "episode";
})(SearchType || (SearchType = {}));
var UserService = /** @class */ (function () {
    /**
     * Constructor of the Service with Dependency Injection
     * @param http The standard Angular HttpClient to make requests
     */
    function UserService(http, storage) {
        this.http = http;
        this.storage = storage;
        this.addtocartdata = [];
    }
    UserService.prototype.setDestn = function (destn) {
        this.category = destn.category;
        this.userdata = destn.user;
        this._loggedIn(destn);
        console.log(this.userdata);
    };
    UserService.prototype._loggedIn = function (resp) {
        this.storage.set('_user', resp);
        this._user = resp.user;
    };
    UserService.prototype.getDestn = function () {
        return this.category;
    };
    UserService.prototype.logout = function () {
        this._user = null;
        this._sell = null;
        this._list = null;
        this.storage.set('_user', null);
    };
    UserService.prototype.addtocart = function (data) {
        this.addtocartdata.push(data);
    };
    UserService.prototype.getcartdata = function () {
        return this.addtocartdata;
    };
    UserService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [HttpClient, Storage])
    ], UserService);
    return UserService;
}());
export { UserService };
//# sourceMappingURL=user.service.js.map