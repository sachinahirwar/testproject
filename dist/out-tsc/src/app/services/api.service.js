import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/share';
var ApiService = /** @class */ (function () {
    //   url: string = 'http://192.168.43.54/unicorn-v2/api/web';
    // url1: string = 'http://192.168.43.54/unicorn-v2/backend/web';
    // apiUrl ='http://localhost/b2bbackend/web/applogin/login';
    function ApiService(http) {
        this.http = http;
        // url: string = 'http://localhost/schoolcrm/api/web';
        // url1: string = 'http://localhost/schoolcrm/backend/web'; 
        // url: string = 'http://192.168.43.51/schoolcrm/api/web';
        // url1: string = 'http://192.168.43.51/schoolcrm/backend/web'; 
        this.url = 'http://216.10.240.90/~prsc/api/web';
        this.url1 = 'http://216.10.240.90/~prsc/backend/web/';
    }
    ApiService.prototype.get = function (endpoint, params, options) {
        if (!options) {
            options = new RequestOptions();
        }
        // Support easy query params for GET requests
        if (params) {
            var p = new URLSearchParams();
            for (var k in params) {
                p.set(k, params[k]);
            }
            // Set the search field if we have params and don't already have
            // a search field set in options.
            options.search = !options.search && p || options.search;
        }
        return this.http.get(this.url1 + '/' + endpoint, options);
    };
    ApiService.prototype.post = function (endpoint, body, options) {
        var chead = new Headers({
            'Content-Type': 'application/x-www-form-urlencoded'
        });
        var coptions = new RequestOptions({
            headers: chead
        });
        var pdata = this._jsonToURLEncoded(body);
        return this.http.post(this.url + '/' + endpoint, pdata, coptions);
    };
    ApiService.prototype.put = function (endpoint, body, options) {
        return this.http.put(this.url + '/' + endpoint, body, options);
    };
    ApiService.prototype.delete = function (endpoint, options) {
        return this.http.delete(this.url + '/' + endpoint, options);
    };
    ApiService.prototype.patch = function (endpoint, body, options) {
        return this.http.put(this.url + '/' + endpoint, body, options);
    };
    //convert a json object to the url encoded format of key=value&anotherkye=anothervalue
    ApiService.prototype._jsonToURLEncoded = function (jsonString) {
        return Object.keys(jsonString).map(function (key) {
            return encodeURIComponent(key) + '=' + encodeURIComponent(jsonString[key]);
        }).join('&');
    };
    ApiService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [Http])
    ], ApiService);
    return ApiService;
}());
export { ApiService };
//# sourceMappingURL=api.service.js.map