import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ApiService } from './../services/api.service';
import { Router, ActivatedRoute } from "@angular/router";
import { UserService } from './../services/user.service';
import { LoadingController } from '@ionic/angular';
var ProfilePage = /** @class */ (function () {
    function ProfilePage(loadingController, router, fb, api, user, route) {
        this.loadingController = loadingController;
        this.router = router;
        this.fb = fb;
        this.api = api;
        this.user = user;
        this.route = route;
        this.mobile = '';
        this.name = '';
        this.username = '';
        console.log(this.user._user);
        this.mobile = this.user._user.mobile;
        this.name = this.user._user.name;
        this.username = this.user._user.username;
        this.profileForm = fb.group({
            name: ['', [Validators.required]],
            mobile: [this.mobile, [Validators.required]],
            username: ['', [Validators.required]],
        });
    }
    ProfilePage.prototype.ngOnInit = function () {
    };
    ProfilePage.prototype.saveprofile = function (profileForm) {
        console.log("profile data", profileForm);
        // this.logincontroller();
        var seq = this.api.post('applogin/updateprofile', profileForm).share();
        seq;
        //       .map(res => res.json())  
        //       .subscribe(res => {
        //         if(res.status == 'success'){ 
        //         console.log(res.user);
        //     this.router.navigate(['/dashboard']);
        //         }else{
        // console.log(res.error);  
        //         }      
        //       }, err => {
        //         console.error('ERROR', err);
        //       });
        return seq;
    };
    ProfilePage = tslib_1.__decorate([
        Component({
            selector: 'app-profile',
            templateUrl: './profile.page.html',
            styleUrls: ['./profile.page.scss'],
        }),
        tslib_1.__metadata("design:paramtypes", [LoadingController,
            Router,
            FormBuilder,
            ApiService,
            UserService,
            ActivatedRoute])
    ], ProfilePage);
    return ProfilePage;
}());
export { ProfilePage };
//# sourceMappingURL=profile.page.js.map