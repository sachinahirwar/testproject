import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from  "@angular/router";
import {Storage} from '@ionic/storage';
import { UserService} from './services/user.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  public appPages = [

     {  
      title: 'dashboard',  
      url: '/dashboard',
      icon: 'grid' 
    }, 
    { 
      title: 'Student Enquiry',
      url: '/studentenquiry',
      icon: 'search'
    },
     { 
      title: 'Unknown Number',
      url: '/incommingcalls',
      icon: 'call'
    },
      
    {
      title: 'followup History',
      url: '/followuphistory', 
      icon: 'person'  
    },

    {
      title: 'View Profile',
      url: '/viewprofile', 
      icon: 'person' 
    },

    // {
    //   title: 'Profile',
    //   url: '/profile', 
    //   icon: 'person' 
    // },

    {
      title: 'Logout',
      url: '/login', 
      icon: 'log-in' 
    },
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private  router:  Router,
    public storage: Storage,
    private user: UserService,
    public navCtrl: NavController

  ) {
    this.initializeApp();
  }  

initializeApp(){
   this.platform.ready().then(() => {

this.storage.get('_user').then((_user) => { 
                if (_user) {
                    this.user._loggedIn(_user);
                          this.navCtrl.navigateRoot("dashboard"); 
                }else{
                  this.navCtrl.navigateRoot(['/login']);
                }
            }, (error) => { 
  
            }); 


      this.statusBar.styleDefault();
      this.statusBar.overlaysWebView(false);
            this.statusBar.backgroundColorByHexString('#0cd1e8');
      this.splashScreen.hide();
    });
  }
 
}
