import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule, Http } from '@angular/http';
import 'rxjs/add/operator/share';
import { ApiService } from './services/api.service';   
import { IonicStorageModule } from '@ionic/storage';
import { UserService } from './services/user.service';
import { Device } from '@ionic-native/device/ngx';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { CallDirectory } from '@ionic-native/call-directory/ngx'; 
import { CallLog } from '@ionic-native/call-log/ngx';
import { MediaCapture } from '@ionic-native/media-capture/ngx';

import { ModalpagePage } from './modalpage/modalpage.page';
import { Contacts} from '@ionic-native/contacts/ngx';   
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
   
import { Camera } from '@ionic-native/Camera/ngx';
import { Base64ToGallery } from '@ionic-native/base64-to-gallery/ngx';
import { Media } from '@ionic-native/media/ngx';
import { File } from '@ionic-native/File/ngx';  
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { Network } from '@ionic-native/network/ngx'; 

 
// import { Chooser } from '@ionic-native/chooser/ngx'; , Contact, ContactField, ContactName 

@NgModule({  
  declarations: [AppComponent,ModalpagePage],
  entryComponents: [ModalpagePage],

  
 //, Contact, ContactField, ContactName
// import { Chooser } from '@ionic-native/chooser/ngx'; 
 
  imports: [
      IonicModule.forRoot(), 
      AppRoutingModule,
      BrowserModule /* or CommonModule */, 
      FormsModule,
      ReactiveFormsModule,
      HttpClientModule,
      HttpModule,

      IonicStorageModule.forRoot()    
    ],
  providers: [  
      StatusBar, 
      ApiService, 
      UserService,
      SplashScreen, 
      CallNumber, 
      CallDirectory,  
      Contacts,
      CallLog,
      Camera,
       File,
       WebView,
       FilePath,
       Base64ToGallery,
       Media,
       Network,
     // Contact,
     // ContactField,
     // ContactName,   
     // Chooser,
    Device,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}  
 