import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IncommingcallsPage } from './incommingcalls.page';

describe('IncommingcallsPage', () => {
  let component: IncommingcallsPage;
  let fixture: ComponentFixture<IncommingcallsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IncommingcallsPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IncommingcallsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
