import { Component, OnInit } from '@angular/core';
import { CallLog, CallLogObject } from '@ionic-native/call-log/ngx';
import { ToastController,NavController,LoadingController,Platform } from '@ionic/angular';
import { Router, ActivatedRoute } from  "@angular/router"; 

@Component({
  selector: 'app-incommingcalls',
  templateUrl: './incommingcalls.page.html',
  styleUrls: ['./incommingcalls.page.scss'],
})
export class IncommingcallsPage implements OnInit {
    private mobile:any = ''; 
 private contactname:any='';
 listTyle:string;
  filters: CallLogObject[];
 private  recordsFound: any; 
  recordsFoundText: string;


  constructor(
  	private callLog: CallLog,
     private platform: Platform,
       private  router:  Router, 
     ) {
  	  this.platform.ready().then(() => {
 
      this.callLog.hasReadPermission().then(hasPermission => {
        if (!hasPermission) {
          this.callLog.requestReadPermission().then(results => {
            this.getContacts("type","1","==");
             console.log('listTyle',this.getContacts("type","1","=="));
          })
            .catch(e => alert(" requestReadPermission " + JSON.stringify(e)));
        } else {
          this.getContacts("type", "1", "==");
        }
      })
        .catch(e => alert(" hasReadPermission " + JSON.stringify(e)));
    });

    }

  ngOnInit() {
  }



    getContacts(name, value, operator) {
      if(value == '1'){
        this.listTyle = "Incoming Calls from yesterday";
         console.log('listTyle',this.listTyle);
      }else if(value == '2'){
        this.listTyle = "Ougoing Calls from yesterday";
      }else if(value == '5'){
        this.listTyle = "Rejected Calls from yesterday";
      }
  
      //Getting Yesterday Time
      var today = new Date();
      var yesterday = new Date(today);
      yesterday.setDate(today.getDate() - 5);  
      var fromTime = yesterday.getTime();
  
      this.filters = [{
        name: name,
        value: value,
        operator: operator,
      }, {
        name: "date",
        value: fromTime.toString(),
        operator: ">=",
      }];
      this.callLog.getCallLog(this.filters)
        .then(results => {
          this.recordsFoundText = JSON.stringify(results);
          this.recordsFound = results;//JSON.stringify(results);
          console.log('recordfound',this.recordsFound);
          console.log('recordfoundTeaat',this.recordsFoundText);
        })
        .catch(e => alert(" LOG " + JSON.stringify(e)));
    }


    goenquiry(number){
    // console.log(name,number);   
      this.router.navigate(['/studentenquiry'], {queryParams: {"mobilenumber": number}});   
    }     


}
