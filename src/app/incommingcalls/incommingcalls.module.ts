import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { IncommingcallsPage } from './incommingcalls.page';

const routes: Routes = [
  {
    path: '',
    component: IncommingcallsPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [IncommingcallsPage]
})
export class IncommingcallsPageModule {}
