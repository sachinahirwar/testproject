import { FormControl, FormGroup, FormBuilder, Validators ,NgForm} from '@angular/forms';
import { ApiService } from './../services/api.service';
import { Router, ActivatedRoute } from  "@angular/router";
import { UserService, SearchType } from './../services/user.service';
import { File, FileEntry } from '@ionic-native/File/ngx';
import { HttpClient } from '@angular/common/http';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { Storage } from '@ionic/storage';
import { FilePath } from '@ionic-native/file-path/ngx';
import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { Camera, CameraOptions, PictureSourceType } from '@ionic-native/Camera/ngx';
import {Http, RequestOptions, Headers, URLSearchParams} from '@angular/http';
import { ActionSheetController, ToastController, Platform, LoadingController,NavController } from '@ionic/angular';
import { finalize } from 'rxjs/operators';



@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
   // url1: string = 'http://localhost/schoolcrm/backend/web'; 
   url1: string = 'http://74.208.88.247/collegecrm/backend/web/'; 

	 profileForm: FormGroup; 
 private mobile:any = ''; 
 private name:any='';
 private username:any='';
 private id:any='';
 private pass:any='';
 image:any;
 currentImage:any;
  img: any;
  filepath: any;
  
  constructor(
     public loadingController: LoadingController,
     private  router:  Router,
     private fb: FormBuilder,
     private api: ApiService,
     private user: UserService,
     private route: ActivatedRoute,
      public toastController: ToastController,
      public actionSheetController: ActionSheetController,
      public navCtrl: NavController,
      private camera: Camera
  	) {
   console.log(this.user._user);

    this.mobile=this.user._user.mobile;
    this.id=this.user._user.id;
    this.name =this.user._user.name;
    this.username =this.user._user.username;
    // this.image = this.url1+"/"+this.user._user.image;
    this.currentImage = this.user._user.image;


   	this.profileForm = fb.group({ 
            id: ['', [Validators.required]],
            name: ['', [Validators.required]],
            mobile: [this.mobile, [Validators.required, Validators.minLength(10), Validators.maxLength(10)]],
            username: ['', [Validators.required]],
            // pass: ['', [Validators.required]],
            image: ['']
        });
   }

  ngOnInit() {
  	 this.filepath = this.api.url1;
  this.img = this.user._user.image;
  this.currentImage = this.user._user.image;
  console.log(this.user._user,'img');
  }

   saveprofile(profileForm){   
    console.log("profile data",profileForm);
    // this.logincontroller();
     let seq = this.api.post('applogin/updateprofile',profileForm).share();
   seq
      .map(res => res.json())  
      .subscribe(res => {
        if(res.status == 'success'){ 
             this.user.setDestn(res);
        console.log(res.user);
        // this.user._user=res.user;
    this.router.navigate(['/dashboard']);
        }else{
console.log(res.error);  
        }      
      }, err => {
        console.error('ERROR', err);
      });

    return seq;
  }

   async presentActionSheet() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Select Image Source',
      buttons: [{
        text: 'Use Gallery',
        handler: () => {
          console.log('Delete clicked');
            this.takePicture();
        }
      }, {
        text: 'Use Camera',
        handler: () => {
          console.log('Favorite clicked');
          this.takePicture1();
        }
      }, {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }]
    });
    await actionSheet.present();
  }



    takePicture() {
        const options: CameraOptions = {
        // quality: 100,
        destinationType: this.camera.DestinationType.DATA_URL,
        encodingType: this.camera.EncodingType.JPEG,
        sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
        mediaType: this.camera.MediaType.PICTURE,
        targetWidth: 300,
        targetHeight: 300

        }

        this.camera.getPicture(options).then((imageData) => {
        // imageData is either a base64 encoded string or a file URI
        // If it's base64 (DATA_URL):
        this.currentImage = 'data:image/jpeg;base64,' + imageData;
        console.log(this.currentImage);
        this.profileForm.patchValue({'image': this.currentImage});
        // this.currentImage = 'data:image/jpeg;base64,' + imageDatas;
        //      this.loginForm.patchValue({'image': this.currentImage});
        }, (err) => {
        // Handle error
        });
    }

    takePicture1() {
        const options: CameraOptions = {
        // quality: 100,
        destinationType: this.camera.DestinationType.DATA_URL,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE,
        targetWidth: 300,
        targetHeight: 300
        }

        this.camera.getPicture(options).then((imageData) => {
        // imageData is either a base64 encoded string or a file URI
        // If it's base64 (DATA_URL):
        this.currentImage = 'data:image/jpeg;base64,' + imageData;
        console.log(this.currentImage);
        this.profileForm.patchValue({'image': this.currentImage});
        let datanew = (<any>window).Ionic.WebView.convertFileSrc(imageData);

        }, (err) => {
        // Handle error
        });
    }

async logincontroller(){
     const loadingElement = await this.loadingController.create({
    message: 'Please wait...',
    spinner: 'crescent',
    duration: 2000
  
  });
     
     await loadingElement.present();
  }


  async presentToast() {

    const toast = await this.toastController.create({
      message: 'Profile Updated Successfully !',
      duration: 2000,
      position: 'top'
    });
    toast.present();

  }


}
