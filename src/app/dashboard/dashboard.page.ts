import { Component, OnInit } from '@angular/core';

import { Router, ActivatedRoute } from  "@angular/router";
import { MenuController } from '@ionic/angular';

import { ApiService } from './../services/api.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {
	pending:any;
  	totalstd:any;
   	notinstd:any;
   	todaysenq:any;

  constructor(
  	private route: ActivatedRoute,
  	private  router:  Router,
  	private api: ApiService,
  	private routers:Router,
  	public menuCtrl: MenuController
  	
  	) {    
  
  	}


   ngOnInit() {
this.menuCtrl.enable(true);
this.getdashboardtotal();

  }

ionViewWillEnter() {
  console.log('ionViewWillEnter dashboard'); 
  this.getdashboardtotal();
}


  getdashboardtotal(){
   console.log("abc");
   let test='dahsboard';
     let seq = this.api.post('applogin/getdashboardtotal',test).share();
   seq
      .map(res => res.json())
      .subscribe(res => {
        if(res.status == 'success'){ 
          this.pending=res.pendingdata;
          this.totalstd=res.totaldata;
          this.notinstd=res.notintrested;
          this.todaysenq = res.todaysenq;

    
        }else{
console.log(res.error);  
        }    

        
      }, err => {
        console.error('ERROR', err);
      });

    return seq;
  }

  todayfollowupstudent(){  
    this.router.navigate(['/todayfollowupstudent']);
  }

   pendingstudent(){  
    this.router.navigate(['/pendingstudent']);
  }

   intrestedstudent(){  
    this.router.navigate(['/intrestedstudent']);
  }

}
