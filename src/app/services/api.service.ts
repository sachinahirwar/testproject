import { Injectable } from '@angular/core';
import {Http, RequestOptions, Headers, URLSearchParams} from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/share';


@Injectable({
  providedIn: 'root'
})
export class ApiService {  

 
   // url: string = 'http://localhost/schoolcrm/api/web';
   // url1: string = 'http://localhost/schoolcrm/backend/web'; 
       

   // url: string = 'http://192.168.43.51/schoolcrm/api/web';
   // url1: string = 'http://192.168.43.51/schoolcrm/backend/web'; 
  

   url: string = 'http://74.208.88.247/collegecrm/api/web'; 
   url1: string = 'http://74.208.88.247/collegecrm/backend/web/';
         
 
    //   url: string = 'http://192.168.43.54/unicorn-v2/api/web';
    // url1: string = 'http://192.168.43.54/unicorn-v2/backend/web';
     // apiUrl ='http://localhost/b2bbackend/web/applogin/login';

    constructor(public http: Http) {  
  
    }

    get(endpoint: string, params?: any, options?: RequestOptions) {
        if (!options) {
            options = new RequestOptions();
        }

        // Support easy query params for GET requests
        if (params) {
            let p = new URLSearchParams();
            for (let k in params) {
                p.set(k, params[k]);
            }
            // Set the search field if we have params and don't already have
            // a search field set in options.
            options.search = !options.search && p || options.search;
        }

        return this.http.get(this.url1 + '/' + endpoint, options);
    }

    post(endpoint: string, body: any, options?: RequestOptions) {

        let chead = new Headers({
            'Content-Type': 'application/x-www-form-urlencoded'
        });
        let coptions = new RequestOptions({
            headers: chead
        });
        var pdata = this._jsonToURLEncoded(body);
        return this.http.post(this.url + '/' + endpoint, pdata, coptions);
    }

    put(endpoint: string, body: any, options?: RequestOptions) {
        return this.http.put(this.url + '/' + endpoint, body, options);
    }

    delete(endpoint: string, options?: RequestOptions) {
        return this.http.delete(this.url + '/' + endpoint, options);
    }

    patch(endpoint: string, body: any, options?: RequestOptions) {
        return this.http.put(this.url + '/' + endpoint, body, options);
    }

    //convert a json object to the url encoded format of key=value&anotherkye=anothervalue
    _jsonToURLEncoded(jsonString) {
        return Object.keys(jsonString).map(function (key) {
            return encodeURIComponent(key) + '=' + encodeURIComponent(jsonString[key]);
        }).join('&');
    }


}
