import { HttpClient } from '@angular/common/http';
import { Observable ,of } from 'rxjs';
import { map ,catchError, tap} from 'rxjs/operators';
import { Injectable } from '@angular/core';
 import {Storage} from '@ionic/storage';
// Typescript custom enum for search types (optional)
export enum SearchType {
  all = '',
  movie = 'movie',
  series = 'series',
  episode = 'episode'  
}
 
@Injectable({
  providedIn: 'root'
})
export class UserService {
  
  private category: any;
  private userdata: any;
    _user: any;
    _sell:any;
    _list:any;
    addtocartdata:any=[];
  /**
   * Constructor of the Service with Dependency Injection
   * @param http The standard Angular HttpClient to make requests
   */
  constructor(private http: HttpClient,public storage: Storage) { }

  public setDestn(destn) {
    this.category = destn.category;
    this.userdata = destn.user;

      this._loggedIn(destn);
    console.log(this.userdata);
  }
    _loggedIn(resp) {
        this.storage.set('_user', resp);

        this._user = resp.user; 
    } 

  getDestn() {
    return this.category;
  }
 
    logout() {
        this._user = null;
         this._sell = null;
         this._list=null;
        this.storage.set('_user', null);
    }


    addtocart(data){
 this.addtocartdata.push(data);

    }

      getcartdata(){
         return this.addtocartdata;
    }
}
