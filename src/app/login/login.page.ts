import { Component, OnInit } from '@angular/core';
import { Router } from  "@angular/router";
import { FormControl, FormGroup, FormBuilder, Validators ,NgForm} from '@angular/forms';
import { UserService, SearchType } from './../services/user.service';
import { ApiService } from './../services/api.service';
import { Observable ,of} from 'rxjs';
import { ToastController,NavController,LoadingController,MenuController } from '@ionic/angular';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/share';
import { map ,catchError, tap} from 'rxjs/operators';
import 'rxjs/Rx';
import { Storage } from '@ionic/storage';
import { Network } from '@ionic-native/network/ngx';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],  
})   
export class LoginPage implements OnInit {  
   loginForm: FormGroup;
    
  constructor(public loadingController: LoadingController,private  router:  Router,private fb: FormBuilder,private userService: UserService,private api: ApiService,
    public toastController: ToastController,
    public navCtrl: NavController,
    private storage: Storage,
    private user: UserService,
    private network: Network,
    public menuCtrl: MenuController) {


  	this.loginForm = fb.group({ 
            name: ['', [Validators.required]],
            password: ['', [Validators.required, Validators.minLength(6)]]
        });

 this.user.logout();


}
  ngOnInit() {
this.menuCtrl.enable(false);



  }


    loginpost(loginForm){
   


// let disconnectSubscription = this.network.onDisconnect().subscribe(() => {
//   console.log('network was disconnected :-(');
//   alert('network was disconnected :-(');
// });
// console.log("network was disconnected");
// // disconnectSubscription.unsubscribe();
// let connectSubscription = this.network.onConnect().subscribe(() => {
//   console.log('network connected!');
//    alert('network connected!');
//   setTimeout(() => {
//     if (this.network.type === 'wifi') {
//       console.log('we got a wifi connection, woohoo!');
//        alert('we got a wifi connection, woohoo!');
//     }
//   }, 1000);
// }); 
// connectSubscription.unsubscribe();
  
    this.network.onConnect().subscribe(() => {
       this.internetconnected();
});  

   this.network.onDisconnect().subscribe(() => {
       this.nointernetconnected();
}); 


    console.log("test",loginForm);
    this.logincontroller();

     let seq = this.api.post('applogin',loginForm).share();
   seq
      .map(res => res.json())
      .subscribe(res => {
        if(res.status == 'success'){ 
      
       //   this.presentToast(); 
           console.log('responce mahesh',res); 
          this.user.setDestn(res);
          console.log('responce',this.user._user);   
           this.router.navigate(['/dashboard']);

    // this.router.navigate(['/dashboard']);
        }else{
           this.loadingController.dismiss();
          this.presentToastError();
console.log(res.error);  
        }    

        
      }, err => {
        console.error('ERROR', err);
      });

    return seq;
  }
 


 async logincontroller(){
     const loadingElement = await this.loadingController.create({
    message: 'Please wait...',
    spinner: 'dots',
    duration: 2000
  
  });
     await loadingElement.present();
  }

 async presentToastError() {
    const toast = await this.toastController.create({
      message: 'Incorrect Username Or Password Please Try Again !',
      duration: 2000,
      position: 'top'
    });
    toast.present();
  } 

 async nointernetconnected() {
    const toast = await this.toastController.create({
      message: 'No Internect Connected !',
      duration: 2000,
      position: 'top'
    });
    toast.present();
  } 

 async internetconnected() {
    const toast = await this.toastController.create({
      message: 'Internect Connected !',
      duration: 2000,
      position: 'top' 
    });
    toast.present();
  }  
  



}
