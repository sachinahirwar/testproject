import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators ,NgForm} from '@angular/forms';
import { UserService, SearchType } from './../services/user.service';
import { ApiService } from './../services/api.service';
import { Observable ,of} from 'rxjs';
import { ToastController,NavController,LoadingController,Platform } from '@ionic/angular';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/share';
import { map ,catchError, tap} from 'rxjs/operators';
import 'rxjs/Rx';
import { Storage } from '@ionic/storage';
import { CallDirectory } from '@ionic-native/call-directory/ngx';
import { Contacts ,Contact, ContactField, ContactName } from '@ionic-native/contacts/ngx';
import { CallLog, CallLogObject } from '@ionic-native/call-log/ngx';
// import { Contacts, Contact, ContactField, ContactName } from '@ionic-native/contacts/ngx';
import { Router, ActivatedRoute } from  "@angular/router";

@Component({ 
  selector: 'app-studentenquiry',
  templateUrl: './studentenquiry.page.html',
  styleUrls: ['./studentenquiry.page.scss'],
})  
export class StudentenquiryPage implements OnInit {
 loginForm: FormGroup; 
 private mobile:any = ''; 
 // private contactname:any='';
 private name:any='';
 listTyle:string;
 filters: CallLogObject[];
 recordsFound: any;
 recordsFoundText: string;
 operators:any;  

   constructor(
     public loadingController: LoadingController,
     private  router:  Router,
     private fb: FormBuilder,
     private userService: UserService,
     private api: ApiService,
    public toastController: ToastController,
    public callDirectory: CallDirectory,
    public navCtrl: NavController,
    private storage: Storage,
    private user: UserService,
    public contacts: Contacts,
    private callLog: CallLog,
     private platform: Platform,
     private route: ActivatedRoute,
    // public contact: Contact,
    // public contactField: ContactField,
    // public contactname: ContactName,
    ) {
    

   // this.name=this.contactname;
   console.log(this.mobile,'mobile number');
     console.log('cantact name3',this.name);
  	this.loginForm = fb.group({ 
            name: ['', [Validators.required]],   
            course: ['', [Validators.required]], 
            mobile: ['', [Validators.required]],
            date: ['', [Validators.required]],
            remark: ['', [Validators.required]],
            operatorid: ['', [Validators.required]]
        }); 

  
    this.route.queryParams.subscribe(params => {
         if(params["mobilenumber"] != ''){
            this.mobile = params["mobilenumber"]; 
            this.name=''; 
           this.loginForm.patchValue({'mobile': this.mobile}); 
           this.loginForm.patchValue({'name': this.name});
           console.log(this.name,'name1');
           
          }       
        });

 
    this.platform.ready().then(() => {
 
      this.callLog.hasReadPermission().then(hasPermission => {
        if (!hasPermission) {
          this.callLog.requestReadPermission().then(results => {
            this.getContacts("type","1","==");
             console.log('listTyle',this.getContacts("type","1","=="));
          })
            .catch(e => alert(" requestReadPermission " + JSON.stringify(e)));
        } else {
          this.getContacts("type", "1", "==");
        }
      })
        .catch(e => alert(" hasReadPermission " + JSON.stringify(e)));
    });
  
}
  ngOnInit() {
   this.getusers();
  }
  


    getContacts(name, value, operator) {
      if(value == '1'){
        this.listTyle = "Incoming Calls from yesterday";
         console.log('listTyle',this.listTyle);
      }else if(value == '2'){
        this.listTyle = "Ougoing Calls from yesterday";
      }else if(value == '5'){
        this.listTyle = "Rejected Calls from yesterday";
      }
  
      //Getting Yesterday Time
      var today = new Date();
      var yesterday = new Date(today);
      yesterday.setDate(today.getDate() - 5);  
      var fromTime = yesterday.getTime();
  
      this.filters = [{
        name: name,
        value: value,
        operator: operator,
      }, {
        name: "date",
        value: fromTime.toString(),
        operator: ">=",
      }];
      this.callLog.getCallLog(this.filters)
        .then(results => {
          this.recordsFoundText = JSON.stringify(results);
          this.recordsFound = results;//JSON.stringify(results);
          console.log('recordfound',this.recordsFound);
          console.log('recordfoundTeaat',this.recordsFoundText);
        })
        .catch(e => alert(" LOG " + JSON.stringify(e)));
    }

   




    saveenquiry(loginForm){   
    console.log("enquiry data",loginForm);
    // this.logincontroller();
     let seq = this.api.post('applogin/savestudentenquiry',loginForm).share();
   seq
      .map(res => res.json())  
      .subscribe(res => {
        if(res.status == 'success'){ 
        console.log(res.user);
    this.router.navigate(['/dashboard']);
        }else{
        console.log(res.error);  
        }       
      }, err => {
        console.error('ERROR', err);
      });

    return seq;
  }
 
 opencalldirector(){ 
 // this.contactname ='';
 //  this.name = '';
  this.contacts.pickContact().then((contact)=>{
    var contactNumber = contact.phoneNumbers[0].value; 
    var contactname=contact.displayName;
    // var contactName=contact.ContactName[0].value;
    this.mobile=contactNumber;  
    // this.contactname=contactname;  
    this.name = contactname; 
    console.log('cantact name1',contactname);
    // alert(contactNumber);  
    // alert(contact); 
    this.loginForm.patchValue({'mobile': this.mobile}); 

    this.loginForm.patchValue({'name': this.name});


  },(err)=>{  
 
    alert(JSON.stringify(err));
     }
     ) 
 }
 
   

 
   getusers(){
     let getstudentid='id';
    let seq = this.api.post('applogin/getusers',getstudentid).share();
   seq
      .map(res => res.json())  
      .subscribe(res => {
        if(res.status == 'success'){ 
          console.log('operators',res.operators);
          this.operators=res.operators;
        }else{
console.log(res.error);    
        }    
        }, err => {
        console.error('ERROR', err);
      });   

    return seq;
  }  

}
