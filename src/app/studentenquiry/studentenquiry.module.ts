import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';

import { StudentenquiryPage } from './studentenquiry.page';

const routes: Routes = [
  {
    path: '',
    component: StudentenquiryPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)

  ],
  declarations: [StudentenquiryPage]
})
export class StudentenquiryPageModule {}
