import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudentenquiryPage } from './studentenquiry.page';

describe('StudentenquiryPage', () => {
  let component: StudentenquiryPage;
  let fixture: ComponentFixture<StudentenquiryPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentenquiryPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudentenquiryPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
