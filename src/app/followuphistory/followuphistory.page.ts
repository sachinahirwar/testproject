import { Component, OnInit } from '@angular/core';
import { ApiService } from './../services/api.service';
import { ModalController } from '@ionic/angular';
import { ModalpagePage } from '../modalpage/modalpage.page';

@Component({
  selector: 'app-followuphistory',
  templateUrl: './followuphistory.page.html',
  styleUrls: ['./followuphistory.page.scss'],
})
export class FollowuphistoryPage implements OnInit {
todayfollowupstudent:any;
followuphistory:any;
  constructor(private api: ApiService, public modal: ModalController) { }

  ngOnInit() {

  	this.getfolloupstudent();
  }
 

    getfolloupstudent(){
     let getstudentid='id'; 
    let seq = this.api.post('applogin/getfolloupstudent',getstudentid).share();
   seq
      .map(res => res.json())  
      .subscribe(res => {
        if(res.status == 'success'){ 
          console.log('operators',res);
          this.todayfollowupstudent=res.todayfollowupstudent;
          this.followuphistory=res.followuphistory;
        }else{
console.log(res.error);   
        }    
        }, err => {
        console.error('ERROR', err);
      });

    return seq;
  }

  async openmodal(id){
    console.log(id);
    const mymodal = await this.modal.create(
      {
        component: ModalpagePage,
        componentProps: {
                         'prop1': id
                      }
    });

    return await mymodal.present();
  }

}
