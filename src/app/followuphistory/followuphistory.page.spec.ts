import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FollowuphistoryPage } from './followuphistory.page';

describe('FollowuphistoryPage', () => {
  let component: FollowuphistoryPage;
  let fixture: ComponentFixture<FollowuphistoryPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FollowuphistoryPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FollowuphistoryPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
