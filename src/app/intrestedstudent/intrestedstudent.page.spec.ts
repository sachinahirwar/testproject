import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IntrestedstudentPage } from './intrestedstudent.page';

describe('IntrestedstudentPage', () => {
  let component: IntrestedstudentPage;
  let fixture: ComponentFixture<IntrestedstudentPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntrestedstudentPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IntrestedstudentPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
