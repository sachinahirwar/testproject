import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { IntrestedstudentPage } from './intrestedstudent.page';

const routes: Routes = [
  {
    path: '',
    component: IntrestedstudentPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [IntrestedstudentPage]
})
export class IntrestedstudentPageModule {}
