import { Component, OnInit } from '@angular/core';
import { Router } from  "@angular/router";
import { FormControl, FormGroup, FormBuilder, Validators ,NgForm} from '@angular/forms';
import { UserService, SearchType } from './../services/user.service';
import { ApiService } from './../services/api.service';
import { Observable ,of} from 'rxjs';
import { ToastController,NavController,LoadingController } from '@ionic/angular';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/share';
import { map ,catchError, tap} from 'rxjs/operators';
import 'rxjs/Rx';
import { Storage } from '@ionic/storage';

@Component({ 
  selector: 'app-todayfollowupstudent',
  templateUrl: './todayfollowupstudent.page.html',
  styleUrls: ['./todayfollowupstudent.page.scss'],
})
export class TodayfollowupstudentPage implements OnInit {
   studentlist:any;
   constructor(public loadingController: LoadingController,private  router:  Router,private fb: FormBuilder,private userService: UserService,private api: ApiService,
    public toastController: ToastController,public navCtrl: NavController,private storage: Storage,private user: UserService) {


  this.gettodayfollowupstudent();
    // console.log('mahesh billore',this.studentlist);
    }

  ngOnInit() {
  }

   gettodayfollowupstudent(){
   let studentstuatus='todaydate';
    let seq = this.api.post('applogin/gettodayfollowupstudent',studentstuatus).share();
   seq
      .map(res => res.json())  
      .subscribe(res => {
        if(res.status == 'success'){ 
        console.log('mahesh billore',res.todayfollowupstudent);
        this.studentlist=res.todayfollowupstudent;
   // this.router.navigate(['/dashboard']);
        }else{
console.log(res.error);   
        }     

        
      }, err => {
        console.error('ERROR', err);
      });

    return seq;
  }

  studentdetail(id){  
    // console.log(id);
      this.router.navigate(['/studentdetail'], {queryParams: {"studentid": id}});
    // this.router.navigate(['/studentdetail']{'id'=>id});
  }

}
