import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { TodayfollowupstudentPage } from './todayfollowupstudent.page';

const routes: Routes = [
  {
    path: '',
    component: TodayfollowupstudentPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [TodayfollowupstudentPage]
})
export class TodayfollowupstudentPageModule {}
