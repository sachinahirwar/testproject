import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TodayfollowupstudentPage } from './todayfollowupstudent.page';

describe('TodayfollowupstudentPage', () => {
  let component: TodayfollowupstudentPage;
  let fixture: ComponentFixture<TodayfollowupstudentPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TodayfollowupstudentPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TodayfollowupstudentPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
