import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PendingstudentPage } from './pendingstudent.page';

describe('PendingstudentPage', () => {
  let component: PendingstudentPage;
  let fixture: ComponentFixture<PendingstudentPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PendingstudentPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PendingstudentPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
