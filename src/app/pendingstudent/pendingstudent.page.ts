import { Component, OnInit } from '@angular/core';
import { Router } from  "@angular/router";
import { FormControl, FormGroup, FormBuilder, Validators ,NgForm} from '@angular/forms';
import { UserService, SearchType } from './../services/user.service';
import { ApiService } from './../services/api.service';
import { Observable ,of} from 'rxjs';
import { ToastController,NavController,LoadingController } from '@ionic/angular';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/share';
import { map ,catchError, tap} from 'rxjs/operators';
import 'rxjs/Rx';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-pendingstudent',
  templateUrl: './pendingstudent.page.html',
  styleUrls: ['./pendingstudent.page.scss'],
})
export class PendingstudentPage implements OnInit {

studentlist:any;

   constructor(
   	public loadingController: LoadingController,
   	private  router:  Router,
   	private fb: FormBuilder,
   	private userService: UserService,
   	private api: ApiService,
    public toastController: ToastController,
    public navCtrl: NavController,
    private storage: Storage,
    private user: UserService) {


  this.getpendingstudent();
    // console.log('mahesh billore',this.studentlist);
    }

  ngOnInit() {
  }


   getpendingstudent(){
   let studentstuatus='pendingstudent';
    let seq = this.api.post('applogin/getpendingstudent',studentstuatus).share();
   seq
      .map(res => res.json())  
      .subscribe(res => {
        if(res.status == 'success'){ 
        console.log('mahesh billore',res.pendingstd);
        this.studentlist=res.pendingstd;
   // this.router.navigate(['/dashboard']);
        }else{
console.log(res.error);   
        }     

        
      }, err => {
        console.error('ERROR', err);
      });

    return seq;
  }

}
