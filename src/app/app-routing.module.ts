import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',  
    pathMatch: 'full'   
  },     

  
  { path: 'studentenquiry', 
  loadChildren: './studentenquiry/studentenquiry.module#StudentenquiryPageModule' 
  },

  { path: 'login', loadChildren: './login/login.module#LoginPageModule' },

   { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardPageModule' },
  { path: 'todayfollowupstudent', loadChildren: './todayfollowupstudent/todayfollowupstudent.module#TodayfollowupstudentPageModule' },
  { path: 'studentdetail', loadChildren: './studentdetail/studentdetail.module#StudentdetailPageModule' },
  { path: 'pendingstudent', loadChildren: './pendingstudent/pendingstudent.module#PendingstudentPageModule' },
  { path: 'intrestedstudent', loadChildren: './intrestedstudent/intrestedstudent.module#IntrestedstudentPageModule' },
  { path: 'profile', loadChildren: './profile/profile.module#ProfilePageModule' },
  { path: 'followuphistory', loadChildren: './followuphistory/followuphistory.module#FollowuphistoryPageModule' },
  { path: 'modalpage', loadChildren: './modalpage/modalpage.module#ModalpagePageModule' },

  { path: 'incommingcalls', loadChildren: './incommingcalls/incommingcalls.module#IncommingcallsPageModule' },  { path: 'viewprofile', loadChildren: './viewprofile/viewprofile.module#ViewprofilePageModule' },




  
];  

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}

