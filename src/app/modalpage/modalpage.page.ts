import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { NavParams } from '@ionic/angular';
import { ApiService } from './../services/api.service';

@Component({
  selector: 'app-modalpage',
  templateUrl: './modalpage.page.html',
  styleUrls: ['./modalpage.page.scss'],
})
export class ModalpagePage implements OnInit {

	@Input() prop1: any;
	value:any;
	id:any;  
  studenthistory:any;
  stdname:any;

	// public value = this.navParams.get('prop1');


  constructor(private api: ApiService, public modal: ModalController, private navParams: NavParams) {
  	// const instance = create(ModalpagePage);
		 //  instance.prop1 = id;
		 console.log(this.navParams.get('prop1'));
		 this.id=this.navParams.get('prop1');
  }  

  ngOnInit() {
  	this.getstudenthistorydetail();   
  }

   getstudenthistorydetail(){
     let getstudentid={id:this.id};
     console.log("jgvhjbhj",getstudentid);
    let seq = this.api.post('applogin/getstudenthistorydetail',getstudentid).share();
   seq
      .map(res => res.json())  
      .subscribe(res => {
        if(res.status == 'success'){ 
          console.log('operators',res);
          if(res!=""){
          this.studenthistory=res.getdata;
          this.stdname=res.getdata[0].name; 
        }
          // this.followuphistory=res.followuphistory;
        }else{
console.log(res.error);   
        }    
        }, err => {
        console.error('ERROR', err);
      });

    return seq;
  }

  closemodal(){
	this.modal.dismiss();
  }

}
