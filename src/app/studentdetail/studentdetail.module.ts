import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { StudentdetailPage } from './studentdetail.page';



const routes: Routes = [
  {
    path: '',
    component: StudentdetailPage
  }
];

@NgModule({
  imports: [ 
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [StudentdetailPage]
})
export class StudentdetailPageModule {}
