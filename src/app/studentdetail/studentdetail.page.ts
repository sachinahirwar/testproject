import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from  "@angular/router";
import { FormControl, FormGroup, FormBuilder, Validators ,NgForm} from '@angular/forms';
import { UserService, SearchType } from './../services/user.service';
import { ApiService } from './../services/api.service';
import { Observable ,of} from 'rxjs';
import { ToastController,NavController,LoadingController } from '@ionic/angular';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/share';
import { map ,catchError, tap} from 'rxjs/operators';
import 'rxjs/Rx';
import { Storage } from '@ionic/storage';
import { CallNumber } from '@ionic-native/call-number/ngx';

@Component({
  selector: 'app-studentdetail',
  templateUrl: './studentdetail.page.html',
  styleUrls: ['./studentdetail.page.scss'],
})

export class StudentdetailPage implements OnInit {
	 loginForm: FormGroup;

    stdname :any;
    stdcourse :any;
    stdmob :any;
    stdid :any;
    returnstdid:any;
    remark:any;
    CallNumber:any;

     private buttonColor: string = "primary"; 

   

  constructor(
  	private route: ActivatedRoute,public loadingController: LoadingController,private  router:  Router,private fb: FormBuilder,private userService: UserService,private api: ApiService,
    public toastController: ToastController,public navCtrl: NavController,private storage: Storage,private user: UserService,private callNumber: CallNumber) { 

  	 this.route.queryParams.subscribe(params => {
            this.stdid = params["studentid"];
            this.getstudentdata(this.stdid); 
            console.log('get student id from todayfollowupstudent',this.stdid);
        });

  	 	this.loginForm = fb.group({ 
             remark: [this.remark, [Validators.required]],
            status: ['', [Validators.required]],
            date: ['', [Validators.required]],
        });

} 

    // this.getstudentdata();

  ngOnInit() {
  }
 ionViewWillEnter() {
  console.log('ionViewWillEnter'); 
}

     studentcall(stmobile){
       this.buttonColor = "success"; 
      console.log("student calling....",stmobile);
      this.callNumber.callNumber(stmobile, true) 
  .then(res => console.log('Launched dialer!', res))
  .catch(err => console.log('Error launching dialer', err));
     }
     

    getstudentdata(id){
     console.log('getstudentdata funtion id',id);
     let getstudentid={id1:id};
    let seq = this.api.post('applogin/getstudentdata',getstudentid).share();
   seq
      .map(res => res.json())  
      .subscribe(res => {
        if(res.status == 'success'){ 
        	this.stdname = res.name;
        	this.stdcourse = res.course;
        	this.stdmob = res.mobile;
          this.remark=res.remark;
        	this.returnstdid = res.id;
        console.log('mahesh billore',this.returnstdid);
        // this.studentlist=res.todayfollowupstudent;
   // this.router.navigate(['/dashboard']);
        }else{
console.log(res.error);   
        }     

        
      }, err => {
        console.error('ERROR', err);
      });

    return seq;
  }


  savestatus(loginForm){
   
    console.log("status data",loginForm);
    console.log("status id",this.returnstdid);
    let loginForm1={'remark':loginForm['remark'],'loginForm':loginForm['status'],'date':loginForm['date'],'stdid':this.stdid};
    // this.logincontroller();
     let seq = this.api.post('applogin/savestatus',loginForm1).share();
   seq
      .map(res => res.json())  
      .subscribe(res => {
        if(res.status == 'success'){ 
        console.log(res.user);
    this.router.navigate(['/dashboard']);
        }else{
console.log(res.error);  
        }     

        
      }, err => {
        console.error('ERROR', err);
      });

    return seq;
  }

}
